@tool
extends Node3D

@onready var grid: Grid = $GridMap 
@onready var cursor: Cursor = $Cursor
@onready var selection: Selection = Selection.new()

@export var draw_debug := false
@export var grid_size: int
@export var color: Color

var plane = Plane(Vector3.ZERO, Vector3.FORWARD, Vector3.RIGHT)

@export var _draw_interval: float
var _timer := 0

var selected_cell = Vector3i(0,0,0)
var input_held = false

func _ready():
  Panku.gd_exprenv.register_env("shipeditor", self)
  selection.set_grid(grid)

func _input(event):
  if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
    input_held = event.pressed
    var intersection = _get_intersection()
    var should_work = not (intersection and event.pressed)
    if intersection and event.pressed:
      selection.start = grid.global_to_map(intersection)
    else:
      selection.reset()

func _process(delta: float) -> void:
  var intersection = _get_intersection()
  if visible:
    DebugDraw3D.draw_grid(
    Vector3(0,0,0), Vector3.FORWARD * grid_size, Vector3.RIGHT * grid_size, Vector2i.ONE * 0.5 * grid_size, color
    )

  if Engine.is_editor_hint(): return
  if not grid: return

  if intersection:
    var map_coords = grid.global_to_map(intersection)
    selection.current = map_coords

  if selection.valid_start:
    cursor.set_selection(
      grid.map_to_global(selection.start, Grid.CellCorner.TopLeft), 
      grid.map_to_global(selection.current, Grid.CellCorner.BottomRight)
    )
  else:
    cursor.set_selection(
      grid.map_to_global(selection.current, Grid.CellCorner.TopLeft),
      grid.map_to_global(selection.current, Grid.CellCorner.BottomRight)
    )

func _get_intersection():
  var mouse = get_viewport().get_mouse_position()
  var projected = %Camera.project_ray_normal(mouse)
  return plane.intersects_ray(%Camera.global_position, projected)


class Selection:

  var start: Vector3i = Vector3i.MAX
  var current: Vector3i

  var valid_start: bool:
    get: return start != Vector3i.MAX

  var _grid
  var global_start: Vector3:
    get:
      return _grid.to_global(_grid.map_to_local(start))
  var global_current: Vector3:
    get:
      return _grid.to_global(_grid.map_to_local(current))

  func set_grid(grid: Grid):
    _grid = grid

  func reset():
    start = Vector3i.MAX
