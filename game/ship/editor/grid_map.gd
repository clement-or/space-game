extends GridMap
class_name Grid 

enum CellCorner {
  Center,
  TopLeft,
  TopRight,
  BottomLeft,
  BottomRight
}

# Given a grid-space coordinate, returns the corresponding world-space coordinate.
# You can specify which corner of the cell you want as a parameter, default is center
func map_to_global(_position: Vector3i, _corner: CellCorner = CellCorner.Center) -> Vector3:
  var glob = to_global(map_to_local(_position))
  var halfsize = cell_size / 2.0
  match _corner:
    CellCorner.TopLeft:
      glob += Vector3(-halfsize.x, 0, -halfsize.z)
    CellCorner.TopRight:
      glob += Vector3(halfsize.x, 0, -halfsize.z)
    CellCorner.BottomLeft:
      glob += Vector3(-halfsize.x, 0, halfsize.z)
    CellCorner.BottomRight:
      glob += Vector3(halfsize.x, 0, halfsize.z)
  return glob

func global_to_map(_position: Vector3) -> Vector3i:
  return local_to_map(to_local(_position))
