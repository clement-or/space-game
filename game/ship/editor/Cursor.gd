extends Node3D
class_name Cursor

@export var top_left: Node3D
@export var top_right: Node3D
@export var bot_left: Node3D
@export var bot_right: Node3D

func _ready():
  assert(top_left and top_right and bot_left and bot_right, "Missing node reference in " + name)

func set_selection(start_pos: Vector3, end_pos: Vector3):
  top_left.global_position = start_pos
  bot_right.global_position = end_pos
  top_right.global_position = Vector3(start_pos.x, start_pos.y ,end_pos.z)
  bot_left.global_position = Vector3(end_pos.x, start_pos.y ,start_pos.z)

